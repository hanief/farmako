@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Articles</h1>

            @foreach ($articles as $article)
                <article>
                        <a href="{{ url('/articles', $article->id) }}"><h2>{{ $article->title }}</h2></a>

                    <div class="body">{{ $article->body }}</div>
                </article>
            @endforeach
        </div>
    </div>
</div>


@stop