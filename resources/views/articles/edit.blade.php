@extends('app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Edit: {!! $article->title !!}</h1>
            {!! Form::model($article, ['method' => 'PATCH', 'action' => ['ArticlesController@update', $article->id]]) !!}
                @include('articles.form', ['submitButtonText' => 'Update Article'])
            {!! Form::close() !!}

            @include('errors.list')
        </div>
    </div>
</div>

@stop