@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h3><span class="glyphicon glyphicon-pencil"></span> Edit materi kuliah <small>{{ $course->title }}</small></h3>
                </div>
                @include('errors.list')
                {!! Form::model($course, ['method' => 'PATCH', 'action' => ['CoursesController@update' , $course->id], 'class' => 'form-horizontal']) !!}
                    @include('course.form', ['submitButtonText' => 'Simpan'])
                {!! Form::close() !!}

            </div>
        </div>
    </div>

    <script src="{{ asset('js/vendor/jquery.ui.widget.js') }}"></script>
    <script src="{{ asset('js/jquery.iframe-transport.js') }}"></script>
    <script src="{{ asset('js/jquery.fileupload.js') }}"></script>
    <script>
        $(function () {
            'use strict';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': '{{ Session::getToken() }}'
                }
            });

            // Initialize the jQuery File Upload widget:
            $('#fileupload').fileupload({
                // Uncomment the following to send cross-domain cookies:
                xhrFields: {withCredentials: true},
                url: '/courses/material',
                formData: {'_token': '{{ Session::getToken() }}'},
                done: function (e, data) {
                    $.each(data.result.files, function (index, file) {
                        var inputFileName =
                                '<div class="input-file-name input-group">' +
                                '<input class="form-control" type="text" value="'+ file.name +'" name="materials[]" readonly>'+
                                '<span class="input-group-btn"><a type="button" class="btn btn-link btn-lg pull-right red remove-material">' +
                                '<span class="glyphicon glyphicon-minus-sign"></span>' +
                                '</a></span>' +
                                '</div>';
                        var section = $('#files').append(inputFileName);
                        section.find('.remove-material').on('click', function() {
                            $(this).parent().parent().remove();
                        });
                    });
                },
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .progress-bar').css(
                            'width',
                            progress + '%'
                    );
                }
            }).prop('disabled', !$.support.fileInput)
                    .parent().addClass($.support.fileInput ? undefined : 'disabled');
        });
    </script>

@stop