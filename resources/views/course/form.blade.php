<div class="form-group">
    {!! Form::label('title', 'Judul:', ['class' => 'col-sm-2']) !!}
    <div class="col-sm-10">
    {!! Form::text('title', $course->title, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('material_file', 'Materi PDF:', ['class' => 'col-sm-2']) !!}
    <div class="col-sm-10">
        <span class="btn btn-success fileinput-button">
            <i class="glyphicon glyphicon-plus"></i>
            <span>Tambah file</span>
            <input id="fileupload" type="file" name="files[]" multiple>
        </span>
        <br/>
        <div id="progress" class="progress">
            <div class="progress-bar progress-bar-success"></div>
        </div>
        <div id="files" class="files">
            @foreach($course->materials as $key => $material)
                <div class="input-file-name input-group">
                    <input class="form-control" type="text" value="{{ $material->material_file }}" name="materials[]" readonly>
                    <span class="input-group-btn">
                        <a type="button" class="btn btn-link btn-lg pull-right red remove-material">
                            <span class="glyphicon glyphicon-minus-sign"></span>
                        </a>
                    </span>
                </div>
            @endforeach
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('videos[]', 'Video URL:', ['class' => 'col-sm-2']) !!}
    <div class="col-sm-10">
        <a class="btn btn-success add-video"><span class="glyphicon glyphicon-plus"></span> Tambah tautan video</a>
        @foreach($course->videos as $video)
            <div class="input-group">
                {!! Form::text('videos[]', $video->video_url, ['class' => 'form-control']) !!}
                <span class="input-group-btn">
                    <a class="btn btn-link btn-lg red remove-video" type="button"><span class="glyphicon glyphicon-minus-sign"></span></a>
                </span>
            </div>
        @endforeach
    </div>
</div>

<div class="form-group">
    {!! Form::label('nano_url', 'Nano URL:', ['class' => 'col-sm-2']) !!}
    <div class="col-sm-10">
        {!! Form::text('nano_url', $course->nano_url, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('nano_cal', 'Nano CAL:', ['class' => 'col-sm-2']) !!}
    <div class="col-sm-10">
        {!! Form::text('nano_cal', $course->nano_cal, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}
    </div>
</div>