@extends('app')

@section('content')
    <div class="container" id="content">
        <div class="row">
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading">Mata Kuliah @if (Auth::check() && Auth::user()->admin) <a class="btn btn-default btn-sm pull-right" href="{{ url('courses/create') }}"><span class="glyphicon glyphicon-plus"></span></a>@endif</div>
                    <table class="table">
                        @foreach ($courses as $course)
                            <tr class="menu-row @if ($course->id == 1) active @endif" id={{ $course->id }}>
                                <td>{{ $course->title }}</td>
                                @if (Auth::check() && Auth::user()->admin)
                                <td><a class="btn btn-default btn-sm" href="{{ url('courses/'.$course->id.'/edit') }}"><span class="glyphicon glyphicon-pencil"></span></a></td>
                                <td><a class="btn btn-default btn-sm" href="{{ url('courses/'.$course->id) }}" data-method="delete" data-token="{{csrf_token()}}" data-confirm="Anda akan menghapus materi kuliah: '{{ $course->title }}'. Lanjutkan?"><span class="glyphicon glyphicon-trash red"></span></a></td>
                                @endif
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="col-md-9">
                <div class="page-header">
                    <h3><span class="glyphicon glyphicon-facetime-video"></span> Video</h3>
                </div>

                <div id="video-embed">

                </div>
                <br>
            </div>
        </div>
    </div>
    <br>
@endsection