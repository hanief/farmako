<div class="form-group">
    {!! Form::label('question', 'Question:') !!}
    {!! Form::text('question', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    {!! Form::file('image', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('option_a', 'Option A:') !!}
    {!! Form::text('option_a', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('option_b', 'Option B:') !!}
    {!! Form::text('option_b', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('option_c', 'Option C:') !!}
    {!! Form::text('option_c', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('option_d', 'Option D:') !!}
    {!! Form::text('option_d', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('option_e', 'Option E:') !!}
    {!! Form::text('option_e', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('answer', 'Answer:') !!}
    {!! Form::text('answer', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}
</div>