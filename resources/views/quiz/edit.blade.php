@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Edit: {{ $quiz->title }}</h1>
                <hr/>
                @include('errors.list')
                {!! Form::model($quiz, ['method' => 'PATCH', 'action' => ['QuizzesController@update' , $quiz->id], 'class' => 'form-horizontal']) !!}
                    @include('quiz.form', ['submitButtonText' => 'Simpan Kuis'])
                {!! Form::close() !!}

            </div>
        </div>
    </div>


@stop