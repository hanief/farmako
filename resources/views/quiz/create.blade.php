@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Buat Kuis</h1>
                <hr/>
                @include('errors.list')
                {!! Form::model($quiz = new App\Quiz, ['url' => 'quizzes', 'class' => 'form-horizontal']) !!}
                    @include('quiz.form', ['submitButtonText' => 'Tambah Kuis'])
                {!! Form::close() !!}
            </div>
        </div>
    </div>


@stop