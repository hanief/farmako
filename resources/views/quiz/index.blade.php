@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Kuis @if (Auth::user()->admin)<a href={{ url('quizzes/create') }} class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Buat baru</a>@endif</h1>
                <hr/>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th>No</th>
                            <th>ID</th>
                            <th>Judul</th>
                            <th>Waktu (menit)</th>
                            <th>Mulai</th>
                            <th>Hasil Tes</th>
                            @if ($user->admin && $user->approved)
                                <th>Pertanyaan</th>
                                <th>Edit</th>
                                <th>Hapus</th>
                            @endif
                        </tr>
                        @foreach($quizzes as $key => $quiz)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $quiz->id }}</td>
                                <td>{{ $quiz->title }}</td>
                                <td>{{ $quiz->time }}</td>
                                <td><a href={{ url('quizzes/'.$quiz->id.'/tests/create') }} class="btn btn-success"><span class="glyphicon glyphicon-play-circle"></span> Mulai</a></td>
                                <td><a href={{ url('quizzes/'.$quiz->id.'/tests') }} class="btn btn-info"><span class="glyphicon glyphicon-stats"></span> Lihat Hasil</a></td>
                                @if (Auth::user()->admin && Auth::user()->approved)
                                    <td><a href={{ url('quizzes/'.$quiz->id.'/questions') }} class="btn btn-warning"><span class="glyphicon glyphicon-question-sign"></span> Pertanyaan</a></td>
                                    <td><a href={{ url('quizzes/'.$quiz->id.'/edit') }} class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></span> Edit</a></td>
                                    <td><a href={{ url('quizzes/'.$quiz->id) }} data-method="delete" data-token="{{csrf_token()}}" data-confirm="Anda yakin?" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Hapus</a></td>
                                @endif
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>


@stop