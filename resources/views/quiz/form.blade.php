<div class="form-group">
    {!! Form::label('title', 'Judul:', ['class' => 'col-sm-2']) !!}
    <div class="col-sm-10">
    {!! Form::text('title', $quiz->title, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('time', 'Waktu (menit):', ['class' => 'col-sm-2']) !!}
    <div class="col-sm-10">
    {!! Form::text('time', $quiz->time, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('course_id', 'Course ID:', ['class' => 'col-sm-2']) !!}
    <div class="col-sm-10">
    {!! Form::text('course_id', $quiz->course_id, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <div class="checkbox">
            <label>
                {!! Form::checkbox('responsi', $quiz->responsi) !!} Responsi
            </label>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}
    </div>
</div>