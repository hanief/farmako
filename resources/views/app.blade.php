<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Computer Assisted Learning.">
    <meta name="author" content="dr. Putryahawa, M.Biomed">
	<title>Farmakologi Fakultas Kedokteran UII</title>

	<link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/slickQuiz.css') }}" media="screen" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/jquery.fileupload.css') }}" rel="stylesheet">
    <script src="{{ asset('/js/jquery.min.js') }}"></script>
    <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<img src="{{ asset('/image/headerbg.jpg') }}" class="img-responsive" alt="Responsive image">
	<nav class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{ url('/') }}">Farmakologi FK UII</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="{{ url('/courses') }}">Kuliah</a></li>
					<li><a href="{{ url('/videos') }}">Video</a></li>
                    <li><a href="{{ url('/cal') }}">CAL</a></li>
                    <li><a href="{{ url('/nanokuis') }}">Kuis</a></li>
				</ul>

				<ul class="nav navbar-nav navbar-right">
					@if (Auth::guest())
						<li><a href="{{ url('/auth/login') }}">Login</a></li>
						<li><a href="{{ url('/auth/register') }}">Register</a></li>
					@else
                        @if (Auth::user()->admin)
                            <li><a href="{{ url('/users') }}">Pengguna</a></li>
                        @endif
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="{{ url('/auth/logout') }}">Logout</a></li>
							</ul>
						</li>
					@endif
				</ul>
			</div>
		</div>
	</nav>

	@yield('content')

    <hr/>
    <p class="text-center">&copy; 2015 Departemen Farmakologi Fakultas Kedokteran UII</p>
	<!-- Scripts -->

	<script src="{{ asset('/js/app.js') }}"></script>
    <script src="{{ url('/js/deleteHandler.js') }}"></script>
</body>
</html>
