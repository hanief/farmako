@extends('app')

@section('content')
<div class="container" id="content">
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">Mata Kuliah</div>
                <ul class="list-group">
                    <li class="list-group-item active" id="1">1. Analgetic and NSAID</li>
                    <li class="list-group-item" id="2">2. Pharmacology in Dyspepsia, GERD and Gastric ulcer</li>
                    <li class="list-group-item" id="3">3. Pharmacotherapy in Heart Failure</li>
                    <li class="list-group-item" id="4">4. Obat-obatan yang mempengaruhi sistem saraf otonom</li>
                    <li class="list-group-item" id="5">5. Obat-obatan pada sistem endokrin</li>
                </ul>
            </div>
        </div>
        <div class="col-md-9"> 
            <div class="page-header">
                <h1>Materi Kuliah</h1>
            </div>
            <div class="embed-responsive embed-responsive-16by9">
                <object id="pdf-object" data="doc/1.pdf" type="application/pdf">
                    <embed src="doc/1.pdf" type="application/pdf" />
                </object>
            </div>
            <!-- <div class="embed-responsive embed-responsive-16by9">
                <object data="pdfobject.html" type="text/html">
                    <embed src="pdfobject.html" type="text/html" />
                </object>
            </div> -->
            <br>
            <form id="pdf-form" method="get" action="doc/1.pdf">
                <button class="btn btn-primary" type="submit"><span class="glyphicon glyphicon-download"></span> Download</button>
            </form>
        </div>
    </div>
</div>
<br>
@endsection