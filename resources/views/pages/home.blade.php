@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Selamat Datang</div>

                <div class="panel-body">
                    <p>Selamat datang di situs Departemen Farmakologi Fakultas Kedokteran UII. Situs ini diperuntukkan sebagai media pembelajaran bagi mahasiswa Fakultas Kedokteran UII dalam menjalani kuliah di Departemen Farmakologi.</p>
                    <h2>Ilmu Farmakologi</h2>
                    <p>Ilmu Farmakologi dewasa ini mengalami perkembangan yang cukup pesat. Selain mempelajari efek suatu obat terhadap tubuh (farmakodinamika) dan bagaimana nasib obat di dalam tubuh (farmakokinetika), Ilmu farmakologi kini juga mendalami mekanisme molekular yang mengakibatkan efek obat dan variasi genetika yang menyebabkan variasi respons pasien terhadap obat (farmakogenetika)</p>
                    <p>Dalam pendidikan kedokteran Ilmu Farmakologi memiliki keunikan tersendiri, karena disiplin ilmu ini menjembatani antara ilmu kedokteran dasar (biomedis) dengan aplikasi klinis di pasien, sehingga tercapai suatu tindakan terapi yang rasional.</p>
                    <h2>Apakah perbedaan antara Ilmu Farmakologi dengan Ilmu Farmasi?</h2>
                    <p>Ilmu Farmakologi dan Ilmu Farmasi mempunyai irisan lapangan yang cukup besar. Namun hal mendasar yang membedakan adalah farmasi mencakup pada sintesis, pembuatan dan proses obat sampai ke tangan pasien (dispensing) dan farmakologi membahas bagaimanakah nasib dan efek obat ketika telah sampai ditubuh pasien.</p>
                    <h2>Tentang kami</h2>
                    <p>Ilmu Farmakologi dan Ilmu Farmasi mempunyai irisan lapangan yang cukup besar. Namun hal mendasar yang membedakan adalah farmasi mencakup pada sintesis, pembuatan dan proses obat sampai ke tangan pasien (dispensing) dan farmakologi membahas bagaimanakah nasib dan efek obat ketika telah sampai ditubuh pasien. </p>
                    <h2>Akademik</h2>
                    <p>Kurikulum farmakologi di tingkat sarjana kedokteran ini menjadi 3 fase:</p>
                    <ol>
                        <li>Farmakologi I dan Farmasi Kedokteran</li>
                        <li>Farmakologi II</li>
                        <li>Farmakologi Klinik</li>
                    </ol>
                    <h2>Penelitian</h2>
                    <p>Penelitian yang sedang berjalan:</p>
                    <p>Perbandingan efek antidiabetik 4 ekstrak bahan alam (jus buah delima, dan ekstrak kulit buah delima)</p>
                    <p>Nanomembran Aloe-centella: terapi baru dalam luka bakar derajat II</p>
                    <h2>Layanan Publik</h2>
                    <ol>
                        <li>Ekstraksi bahan alam</li>
                        <li>Uji Toksikologi Akut</li>
                    </ol>
                    <h2>Struktur Departemen</h2>
                    <p>Kepala Departemen: dr. Putryahawa, M.Biomed</p>
                    <p>Staf:</p>
                    <ul>
                        <li>dr. Isnatin Miladiyah, M.Kes</li>
                        <li>dr. Riana Rachmawati, M.Kes</li>
                        <li>dr. Sufi Desrini, M.Sc</li>
                        <li>Erna Dita, Amd</li>
                    </ul>
                    <h2>Kontak</h2>
                    <p>Untuk informasi dan keterangan lebih lanjut silahkan menghubungi :<br/>
                        Departemen Farmakologi <br/>
                        Fakultas Kedokteran Universitas Islam Indonesia <br/>
                        Jl. Kaliurang Km 14,5 Yogyakarta 55584 <br/>
                        Telepon : (0274) 898580 ext 2060 <br/>
                        Fax : (0274) 898580 ext 2007 <br/>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection