@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Kuis</div>

                <div class="panel-body">
                    <div id="slick-quiz">
                        <h1 class="quizName"></h1>

                        <div class="quizArea">
                            <div class="quizHeader">
                                <a class="button startQuiz" href="#">Get Started!</a>
                            </div>
                        </div>

                        <div class="quizResults">
                            <h3 class="quizScore">You Scored: <span></span></h3>

                            <h3 class="quizLevel"><strong>Ranking:</strong> <span></span></h3>

                            <div class="quizResultsCopy">
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection