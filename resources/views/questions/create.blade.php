@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Buat Kuis</h1>
                <hr/>
                @include('errors.list')
                {!! Form::model($question = new App\Question, ['url' => 'quizzes/'.$quizId.'/questions', 'class' => 'form-horizontal']) !!}
                    {!! Form::hidden('quiz_id', $quizId) !!}
                    @include('questions.form', ['submitButtonText' => 'Tambah Pertanyaan'])
                {!! Form::close() !!}
            </div>
        </div>
    </div>


@stop