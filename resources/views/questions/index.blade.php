@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Pertanyaan <a href={{ url('quizzes/'.$quiz->id.'/questions/create') }} class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Buat baru</a></h1>
                <hr/>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th>Judul Pertanyaan</th>
                            <th>Edit</th>
                            <th>Hapus</th>
                        </tr>
                        @foreach($questions as $question)
                            <tr>
                                <td>{{ $question->question }}</td>
                                <td><a href={{ url('quizzes/'.$quiz->id.'/questions/'.$question->id.'/edit') }} class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></span> Edit</a></td>
                                <td><a href={{ url('quizzes/'.$quiz->id.'/questions/'.$question->id) }} data-method="delete" data-token="{{csrf_token()}}" data-confirm="Anda yakin?" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Hapus</a></td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>


@stop