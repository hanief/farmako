@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-offset-2 col-md-8">
                <h1>{{ $quiz->title }}</h1>
                <h4>Durasi: {{ $quiz->time }} menit</h4>
                <hr/>
                {!! Form::open(['url' => 'quizzes/'.$quiz->id.'/tests/'.$test->id.'/answers']) !!}
                    @foreach($questions as $key => $question)
                        <h6>{{ $key+1 }}. {{ $question->question }}</h6>
                        <div class="radio">
                            <label>
                                <input type="radio" name="{{$question->id}}" id="option_a" value="a">
                                {{ $question->option_a }}
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="{{$question->id}}" id="option_b" value="b">
                                {{ $question->option_b }}
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="{{$question->id}}" id="option_c" value="c">
                                {{ $question->option_c }}
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="{{$question->id}}" id="option_d" value="d">
                                {{ $question->option_d }}
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="{{$question->id}}" id="option_e" value="e">
                                {{ $question->option_e }}
                            </label>
                        </div>
                        <br/>
                    @endforeach
                    {!! Form::submit('Selesai', ['class' => 'btn btn-primary form-control']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>


@stop