@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Edit: {{ $question->question }}</h1>
                <hr/>
                @include('errors.list')
                {!! Form::model($question, ['method' => 'PATCH', 'url' => 'quizzes/'.$quizId.'/questions/'.$question->id, 'class' => 'form-horizontal']) !!}
                    @include('questions.form', ['submitButtonText' => 'Simpan Pertanyaan'])
                {!! Form::close() !!}

            </div>
        </div>
    </div>


@stop