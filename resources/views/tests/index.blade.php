@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Pertanyaan</h1>
                <hr/>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th>Nama</th>
                            <th>Kuis</th>
                            <th>Mulai Tes</th>
                            <th>Selesai Tes</th>
                            <th>Jawaban</th>
                        </tr>
                        @foreach($tests as $test)
                            <tr>
                                <td>{{ $test->user_id }}</td>
                                <td>{{ $test->quiz_id }}</td>
                                <td>{{ $test->start_at }}</td>
                                <td>{{ $test->end_at }}</td>
                                <td><a href={{ url('quizzes/'.$quiz->id.'/tests/'.$test->id) }} class="btn btn-primary">Lihat Jawaban</a></td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>


@stop