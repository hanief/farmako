<div class="form-group">
    {!! Form::label('question', 'Judul Pertanyaan:', ['class' => 'col-sm-2']) !!}
    <div class="col-sm-10">
        {!! Form::text('question', $question->question, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('option_a', 'Opsi A:', ['class' => 'col-sm-2']) !!}
    <div class="col-sm-10">
        {!! Form::text('option_a', $question->option_a, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('option_b', 'Opsi B:', ['class' => 'col-sm-2']) !!}
    <div class="col-sm-10">
        {!! Form::text('option_b', $question->option_b, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('option_c', 'Opsi C:', ['class' => 'col-sm-2']) !!}
    <div class="col-sm-10">
        {!! Form::text('option_c', $question->option_c, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('option_d', 'Opsi D:', ['class' => 'col-sm-2']) !!}
    <div class="col-sm-10">
        {!! Form::text('option_d', $question->option_d, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('option_e', 'Opsi E:', ['class' => 'col-sm-2']) !!}
    <div class="col-sm-10">
        {!! Form::text('option_e', $question->option_e, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('answer', 'Jawaban:', ['class' => 'col-sm-2']) !!}
    <div class="col-sm-10">
        {!! Form::text('answer', $question->answer, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}
    </div>
</div>