@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Daftar Pengguna</h1>
                <hr/>
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>NIM</th>
                            <th>Email</th>
                            <th>Approved</th>
                            <th>Admin</th>
                        </tr>
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->nim }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-default @if ($user->approved) active @endif" >
                                            <input class="approve-button" type="radio" name="{{ $user->id }}" id="option1" autocomplete="off" @if ($user->approved) checked @endif> YES
                                        </label>
                                        <label class="btn btn-default @if (!$user->approved) active @endif">
                                            <input class="approve-button" type="radio" name="{{ $user->id }}" id="option2" autocomplete="off" @if (!$user->approved) checked @endif> NO
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-default @if ($user->admin) active @endif" >
                                            <input class="adminize-button" type="radio" name="{{ $user->id }}" id="option1" autocomplete="off" @if ($user->admin) checked @endif> YES
                                        </label>
                                        <label class="btn btn-default @if (!$user->admin) active @endif">
                                            <input class="adminize-button" type="radio" name="{{ $user->id }}" id="option2" autocomplete="off" @if (!$user->admin) checked @endif> NO
                                        </label>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': '{{ Session::getToken() }}'
                }
            });

            $('.approve-button').change(function() {
                $.post( '/users/'+$(this).attr('name')+'/approve', { '_token': '{{ Session::getToken() }}' }, function(data) {
                    console.log(data);
                    if (data.user.approved) {

                    } else {

                    }
                });
            });

            $('.adminize-button').change(function() {
                $.post( '/users/'+$(this).attr('name')+'/adminize', { '_token': '{{ Session::getToken() }}' }, function(data) {
                    console.log(data);
                    if (data.user.admin) {

                    } else {

                    }
                });
            });
        });
    </script>
@stop