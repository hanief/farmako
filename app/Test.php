<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model {

    protected $fillable = [
        'quiz_id',
        'user_id',
        'start_at',
        'end_at',
        'score'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function quiz()
    {
        return $this->belongsTo('App\Quiz');
    }

    public function answers()
    {
        return $this->hasMany('App\Answer');
    }
}
