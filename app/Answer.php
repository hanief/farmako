<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model {

    protected $fillable = [
        'quiz_id',
        'user_id',
        'test_id',
        'question_id',
        'answer'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function quiz()
    {
        return $this->belongsTo('App\Quiz');
    }

    public function test()
    {
        return $this->belongsTo('App\Test');
    }

    public function question()
    {
        return $this->belongsTo('App\Question');
    }

}
