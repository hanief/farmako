<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'nim', 'email', 'password', 'approved', 'admin'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

    /**
     * A user can have many articles
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function articles()
    {
        return $this->hasMany('App\Article');
    }

    public function isAdmin()
    {
        return $this->attributes['admin'];
    }

    public function isApproved()
    {
        return $this->attributes['approved'];
    }

    public function quizzes()
    {
        return $this->hasMany('App\Quiz');
    }

    public function questions()
    {
        return $this->hasMany('App\Question');
    }

    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    public function tests()
    {
        return $this->hasMany('App\Test');
    }

    public function courses()
    {
        return $this->hasMany('App\Course');
    }

    public function videos()
    {
        return $this->hasMany('App\Video');
    }

    public function materials()
    {
        return $this->hasMany('App\Material');
    }
}
