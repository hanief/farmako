<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Material extends Model {

    protected $fillable = [
        'course_id',
        'user_id',
        'material_url',
        'material_file'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function course()
    {
        return $this->belongsTo('App\Course');
    }

}
