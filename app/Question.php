<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model {

    protected $fillable = [
        'question',
        'image_url',
        'option_a',
        'option_b',
        'option_c',
        'option_d',
        'option_e',
        'answer',
        'quiz_id',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function quiz()
    {
        return $this->belongsTo('App\Quiz');
    }

    public function answers()
    {
        return $this->hasMany('App\Answer');
    }
}
