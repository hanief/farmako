<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model {

    protected $fillable = [
        'course_id',
        'user_id',
        'video_url',
        'video_file'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function course()
    {
        return $this->belongsTo('App\Course');
    }
}
