<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Quiz;
use Illuminate\Http\Request;
use Auth;

/**
 * Class QuizzesController
 * @package App\Http\Controllers
 */
class QuizzesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
    {
        $quizzes = Quiz::latest()->get();

        return view('quiz.index')->with(['quizzes' => $quizzes, 'user' => Auth::user()]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view('quiz.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Requests\QuizRequest $request)
	{
        $this->createQuiz($request);

		return redirect('quizzes');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $quiz = Quiz::findOrFail($id);

        return view('quiz.show')->with('quiz', $quiz);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $quiz = Quiz::findOrFail($id);

        return view('quiz.edit')->with('quiz', $quiz);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Requests\QuizRequest $request)
	{
        $quiz = Quiz::findOrFail($id);

        $quiz->update($request->all());

		return redirect('quizzes');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $quiz = Quiz::findOrFail($id);
        $quiz->delete();

		return redirect('quizzes');
	}

    private function createQuiz(Requests\QuizRequest $request)
    {
        $quiz = Auth::user()->quizzes()->create($request->all());

        return $quiz;
    }
}
