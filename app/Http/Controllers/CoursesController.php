<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Course;
use Auth;
use Illuminate\Support\Facades\App;
use Input;

class CoursesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $courses = Course::oldest()->get();

        return view('course.index')->with(['courses' => $courses, 'user' => Auth::user()]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $course = new Course;
		return view('course.create')->with('course', $course);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Requests\CourseRequest $request)
	{
        $course = new Course($request->all());

        if (Auth::check()) {
            $course = $this->createCourse($request);
            $this->createMaterials($request, $course);

            return redirect('courses');
        }
        return redirect('courses/create')->with('course', $course);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return redirect('/courses');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $course = Course::with('materials', 'videos')->findOrFail($id);

        return view('course.edit')->with('course', $course);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Requests\CourseRequest $request)
	{
        $course = Course::findOrFail($id);
        if (Auth::check()) {
            $course->update($request->all());
            $this->updateMaterials($request, $course);
            $this->updateVideos($request, $course);
        }
        return redirect('courses');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $course = Course::findOrFail($id);
        $course->delete();

        return redirect('courses');
	}

    public function allCourses()
    {
        return Course::oldest()->with('videos', 'materials')->get();
    }

    public function saveMaterialFiles()
    {
        $files = Input::file('files');
        $assetPath = '/doc';
        $uploadPath = public_path($assetPath);
        $results = array();

        foreach ($files as $file) {
            $modifiedFileName = date('y-m-d-His-').$file->getClientOriginalName();
            $file->move($uploadPath, $modifiedFileName);
            $name = $assetPath . '/' . $modifiedFileName;
            $results[] = compact('name');
        }

        return array(
            'files' => $results
        );
    }

    public function video()
    {
        $courses = Course::oldest()->with('videos')->get();

        return view('course.video')->with('courses', $courses);
    }

    public function cal()
    {

    }

    public function quiz()
    {

    }

    private function createCourse(Requests\CourseRequest $request)
    {
        $course = Auth::user()->courses()->create($request->all());

        return $course;
    }

    private function createMaterials($request, $course)
    {
        $materials = [];

        foreach ($request['materials'] as $material) {
            $material = Auth::user()->materials()->create([
                'course_id' => $course->id,
                'material_file' => $material,
                'material_url' => ''
            ]);

            $materials[] = $material;
        }

        return $materials;
    }

    private function updateMaterials($request, $course)
    {
        $materials = $course->materials()->get();
        $newMaterials = [];

        foreach ($materials as $material) {
            if (in_array($material, $request['materials'])) {
                $newMaterials[] = $material;
            } else {
                $material->delete();
            }
        }

        foreach ($request['materials'] as $material) {
            if (!in_array($material, $materials->toArray())) {
                $newMaterial = Auth::user()->materials()->create([
                    'course_id' => $course->id,
                    'material_file' => $material,
                    'material_url' => ''
                ]);
                $newMaterials[] = $newMaterial;
            }
        }

        return $newMaterials;
    }

    private function updateVideos($request, $course)
    {
        $videos = $course->videos()->get();
        $newVideos = [];

        foreach ($videos as $video) {
            if (in_array($video, $request['videos'])) {
                $newVideos[] = $video;
            } else {
                $video->delete();
            }
        }

        foreach ($request['videos'] as $video) {
            if (!in_array($video, $videos->toArray())) {
                $newVideo = Auth::user()->videos()->create([
                    'course_id' => $course->id,
                    'video_url' => $video,
                    'video_file' => ''
                ]);
                $newVideos[] = $newVideo;
            }
        }

        return $newVideos;
    }
}
