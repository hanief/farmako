<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class PagesController extends Controller {

	public function home() 
	{
		return view('pages.home');
	}

	public function kuliah() 
	{
		return view('pages.kuliah');
	}

	public function kuis() 
	{
		return view('pages.kuis');
	}

	public function responsi() 
	{
		return view('pages.responsi');
	}

	public function video() 
	{
		return view('pages.video');
	}
}
