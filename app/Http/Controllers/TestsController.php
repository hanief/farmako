<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Quiz;
use App\Test;
use App\Answer;
use Auth;
use Carbon\Carbon;

class TestsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($quizId)
	{
        $quiz = Quiz::findOrFail($quizId);
        $tests = [];

        if (Auth::user()->admin) {
            $tests = $quiz->tests()->get();
        } else {
            $tests = Test::where(['quiz_id' => $quizId, 'user_id' => Auth::user()->id]);
        }

        return view('tests.index')->with(['quiz' => $quiz, 'tests' => $tests]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($quizId, Requests\TestRequest $request)
	{
        $test = $this->createTest($quizId, $request);

        return redirect('quizzes/'.$quizId.'/tests/'.$test->id);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		return abort(404);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($quizId, $testId)
	{
        $test = Test::findOrFail($testId);
        $quiz = Quiz::findOrFail($quizId);
        $questions = $quiz->questions()->get();
//        $shuffledQuestions = shuffle($questions);

        return view('tests.show')->with(['test' => $test, 'quiz' => $quiz, 'questions' => $questions]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($quizId, $testId)
	{
        return redirect('quizzes/'.$quizId.'/tests/'.$testId);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($quizId)
	{
        return abort(404);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($quizId)
	{
        return abort(404);
	}

    public function answer($quizId, $testId, Requests\TestRequest $request)
    {
        $quiz = Quiz::findOrFail($quizId);
        $test = Test::findOrFail($testId);
        $test->end_at = Carbon::now();
        $test->save();

        $questions = $quiz->questions()->get();
        $rightAnswer = 0;

        foreach ($questions as $question) {

            $answer = Auth::user()->answers()->create([
                'answer' => $request[$question->id],
                'quiz_id' => $quiz->id,
                'test_id' => $testId,
                'question_id' => $question->id
            ]);

            if ($answer == $question->answer) {
                $rightAnswer++;
            }
        }

        $score = $rightAnswer/count($questions);
        $test->score = $score;
        $test->save();

        return redirect('quizzes');
    }

    private function createTest($quizId, Requests\TestRequest $request)
    {
        if (Auth::check()) {
            $test = Auth::user()->tests()->create([
                'score' => 0,
                'start_at' => Carbon::now()
            ]);

            Quiz::find($quizId)->tests()->save($test);

            return $test;
        }

        return null;
    }

}
