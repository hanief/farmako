<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Quiz;
use App\Question;
use Auth;

/**
 * Class QuestionsController
 * @package App\Http\Controllers
 */
class QuestionsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($quizId)
	{
        $quiz = Quiz::findOrFail($quizId);
        $questions = $quiz->questions()->get();

		return view('questions.index')->with(['quiz' => $quiz, 'questions' => $questions]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($quizId)
	{
		return view('questions.create')->with('quizId', $quizId);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($quizId, Requests\QuestionRequest $request)
	{
        $this->createQuestion($request, $quizId);

		return redirect('/quizzes/'.$quizId.'/questions');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($quizId, $questionId)
	{
		return redirect('/quizzes/'.$quizId.'/questions/'.$questionId.'/edit');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($quizId, $questionId)
	{
        $question = Question::findOrFail($questionId);

		return view('questions.edit')->with(['question' => $question, 'quizId' => $quizId]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $questionId
	 * @return Response
	 */
	public function update($quizId, $questionId, Requests\QuestionRequest $request)
	{
        $question = Question::findOrFail($questionId);

        $question->update($request->all());

		return redirect('/quizzes/'.$quizId.'/questions');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($quizId, $questionId)
	{
        $question = Question::findOrFail($questionId);
        $question->delete();

        return redirect('/quizzes/'.$quizId.'/questions');
	}


    private function createQuestion(Requests\QuestionRequest $request, $quizId)
    {
        $question = Auth::user()->questions()->create($request->all());
        $quiz = Quiz::find($quizId);
        $quiz->questions()->save($question);

        return $question;
    }
}
