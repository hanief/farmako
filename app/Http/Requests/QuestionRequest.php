<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class QuestionRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'question' => 'required|min:3',
            'option_a' 	=> 'required|min:1',
            'option_b' 	=> 'required|min:1',
            'option_c' 	=> 'required|min:1',
            'option_d' 	=> 'required|min:1',
            'option_e' 	=> 'required|min:1',
            'answer'	=> 'required|min:1|max:1'
		];
	}

}
