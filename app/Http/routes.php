<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'PagesController@home');
Route::get('home', 'PagesController@home');
Route::get('kuliah', 'CoursesController@index');
Route::get('kuis', 'PagesController@kuis');
Route::get('responsi', 'PagesController@responsi');
Route::get('videos', 'CoursesController@video');
Route::get('nanokuis', 'PagesController@nanokuis');

Route::post('quizzes/{quizId}/tests/{testId}/answers', 'TestsController@answer');
Route::post('users/{userId}/approve', 'UsersController@approve');
Route::post('users/{userId}/adminize', 'UsersController@adminize');
Route::get('courses/all', 'CoursesController@allCourses');
Route::post('courses/material', 'CoursesController@saveMaterialFiles');

Route::resource('articles', 'ArticlesController');
Route::resource('users', 'UsersController');
Route::resource('quizzes', 'QuizzesController');
Route::resource('courses', 'CoursesController');

Route::resource('quizzes/{quizId}/questions', 'QuestionsController');

Route::resource('quizzes/{quizId}/tests', 'TestsController');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
