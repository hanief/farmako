<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model {

    protected $fillable = [
        'title',
        'nano_url',
        'nano_cal',
        'user_id'
    ];

    public function quizzes()
    {
        return $this->hasMany('App\Quiz');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function videos()
    {
        return $this->hasMany('App\Video');
    }

    public function materials()
    {
        return $this->hasMany('App\Material');
    }
}
