<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model {

    protected $fillable = [
        'title',
        'time',
        'course_id',
        'user_id',
        'responsi'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function course()
    {
        return $this->belongsTo('App\User');
    }

    public function questions()
    {
        return $this->hasMany('App\Question');
    }

    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    public function tests()
    {
        return $this->hasMany('App\Test');
    }
}
