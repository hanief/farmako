<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Quiz;
use App\Question;
use App\Course;
use App\Video;
use App\Material;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
//		Model::unguard();

		$this->call('UserTableSeeder');

        $this->command->info('User table seeded!');

        $this->call('QuizTableSeeder');

        $this->command->info('Quiz table seeded!');

        $this->call('QuestionTableSeeder');

        $this->command->info('Question table seeded!');

        $this->call('CourseTableSeeder');

        $this->command->info('Course table seeded!');

        $this->call('VideoTableSeeder');

        $this->command->info('Video table seeded!');

        $this->call('MaterialTableSeeder');

        $this->command->info('Material table seeded!');
	}

}

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        User::create([
            'email' => 'hanief@gmail.com',
            'name'  => 'hanief',
            'nim'   => '12345',
            'password' => Hash::make('password'),
            'approved' => true,
            'admin' => true
        ]);

        User::create([
            'email' => 'hanief@cahyautama.com',
            'name'  => 'cahya',
            'nim'   => '23456',
            'password' => Hash::make('password'),
            'approved' => true,
            'admin' => false
        ]);

        User::create([
            'email' => 'dr.putrya@gmail.com',
            'name'  => 'Putrya',
            'nim'   => '0971101023',
            'password' => '$2y$10$.wnfNTtuLl905x0q0YoRoep212cPxnxiTAC7ITlLxJ.aAZMts9Qt.',
            'approved' => true,
            'admin' => true
        ]);
    }

}

class QuizTableSeeder extends Seeder {

    public function run()
    {
        DB::table('quizzes')->delete();

        Quiz::create([
            'title' => 'Who wants to be a millionaire',
            'time'  => 60,
            'user_id'   => 1,
            'course_id' => 1,
            'responsi' => false
        ]);

        Quiz::create([
            'title' => 'Family Feud',
            'time'  => 100,
            'user_id'   => 1,
            'course_id' => 1,
            'responsi' => false
        ]);
    }

}

class QuestionTableSeeder extends Seeder {

    public function run()
    {
        DB::table('questions')->delete();

        Question::create([
            'question' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam euismod, odio vel bibendum accumsan, turpis sem volutpat dui, nec dapibus tellus quam ac risus?',
            'option_a' => 'Nam convallis',
            'option_b' => 'Orci finibus cursus ornare',
            'option_c' => 'Dui tortor euismod nibh',
            'option_d' => 'Vel gravida elit enim in ex',
            'option_e' => 'Quisque in erat vestibulum',
            'answer'   => 'e',
            'user_id'   => 1,
            'quiz_id'   => 1,
            'image_url' => ''
        ]);

        Question::create([
            'question' => 'Cras efficitur rhoncus suscipit. Suspendisse finibus non lectus in ullamcorper. Ut mattis ante eu eros tempus, non suscipit metus volutpat?',
            'option_a' => 'Nam convallis',
            'option_b' => 'Orci finibus cursus ornare',
            'option_c' => 'Dui tortor euismod nibh',
            'option_d' => 'Vel gravida elit enim in ex',
            'option_e' => 'Quisque in erat vestibulum',
            'answer'   => 'b',
            'user_id'   => 1,
            'quiz_id'   => 1,
            'image_url' => ''
        ]);
    }
}

class CourseTableSeeder extends Seeder {

    public function run()
    {
        DB::table('courses')->delete();

        Course::create([
            'title' => 'Analgetic and NSAID',
            'nano_url' => 'http://kuliah.fkuii.org/lms/mod/quiz/view.php?id=402',
            'nano_cal' => 'swf/1.swf',
            'user_id' => 1
        ]);

        Course::create([
            'title' => 'Pharmacology in Dyspepsia, GERD and Gastric Ulcer',
            'nano_url' => 'http://kuliah.fkuii.org/lms/mod/quiz/view.php?id=401',
            'nano_cal' => '',
            'user_id' => 1
        ]);

        Course::create([
            'title' => 'Pharmacotherapy in Heart Failure',
            'nano_url' => '',
            'nano_cal' => '',
            'user_id' => 1
        ]);

        Course::create([
            'title' => 'Obat-obatan yang mempengaruhi sistem saraf otonom',
            'nano_url' => '',
            'nano_cal' => '',
            'user_id' => 1
        ]);

        Course::create([
            'title' => 'Obat-obatan pada sistem endokrin',
            'nano_url' => '',
            'nano_cal' => '',
            'user_id' => 1
        ]);
    }
}

class VideoTableSeeder extends Seeder {

    public function run()
    {
        DB::table('videos')->delete();

        Video::create([
            'user_id' => 1,
            'course_id' => 1,
            'video_url' => 'http://www.youtube.com/embed/KWEYCJIyUEI',
            'video_file' => 'video/1.mp4'
        ]);

        Video::create([
            'user_id' => 1,
            'course_id' => 2,
            'video_url' => '',
            'video_file' => ''
        ]);

        Video::create([
            'user_id' => 1,
            'course_id' => 3,
            'video_url' => 'http://www.youtube.com/embed/mhYeO2fwSps',
            'video_file' => 'video/3.mp4'
        ]);

        Video::create([
            'user_id' => 1,
            'course_id' => 4,
            'video_url' => 'http://www.youtube.com/embed/hJh4tMCGVrM',
            'video_file' => ''
        ]);

        Video::create([
            'user_id' => 1,
            'course_id' => 4,
            'video_url' => 'http://www.youtube.com/embed/r92dHDxJhYo',
            'video_file' => ''
        ]);

        Video::create([
            'user_id' => 1,
            'course_id' => 4,
            'video_url' => 'http://www.youtube.com/embed/o4Srx4mUmaI',
            'video_file' => ''
        ]);

        Video::create([
            'user_id' => 1,
            'course_id' => 5,
            'video_url' => 'http://www.youtube.com/embed/C9XYnZdEIPE',
            'video_file' => ''
        ]);

        Video::create([
            'user_id' => 1,
            'course_id' => 5,
            'video_url' => 'http://www.youtube.com/embed/c6s6SZ9Vnbk',
            'video_file' => ''
        ]);

        Video::create([
            'user_id' => 1,
            'course_id' => 5,
            'video_url' => 'http://www.youtube.com/embed/uCjpGlnCjeA',
            'video_file' => ''
        ]);

        Video::create([
            'user_id' => 1,
            'course_id' => 5,
            'video_url' => 'http://www.youtube.com/embed/sD9st1ZPFrQ',
            'video_file' => ''
        ]);
    }
}

class MaterialTableSeeder extends Seeder {

    public function run()
    {
        DB::table('materials')->delete();

        Material::create([
            'user_id' => 1,
            'course_id' => 1,
            'material_url'   => '',
            'material_file' => 'doc/1.pdf'
        ]);

        Material::create([
            'user_id' => 1,
            'course_id' => 2,
            'material_url'   => '',
            'material_file' => 'doc/2.pdf'
        ]);

        Material::create([
            'user_id' => 1,
            'course_id' => 3,
            'material_url'   => '',
            'material_file' => 'doc/3.pdf'
        ]);

        Material::create([
            'user_id' => 1,
            'course_id' => 4,
            'material_url'   => '',
            'material_file' => 'doc/4.pdf'
        ]);

        Material::create([
            'user_id' => 1,
            'course_id' => 5,
            'material_url'   => '',
            'material_file' => 'doc/5.pdf'
        ]);
    }
}