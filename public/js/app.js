$(document).ready(function() {
    var courses;

    $.get("/courses/all", function (data) {
        courses = data;
        changeVideos(courses[0]);
        changeMaterials(courses[0]);
    });

    var changeVideos = function(course){
        $('#video-embed .embed-responsive').remove();
        $('#video-embed .error-text').remove();
        $('#video-embed br').remove();
        if (course.videos.length > 0) {
            course.videos.forEach(function (element) {
                var newVideoEmbed = "<div class='embed-responsive embed-responsive-16by9'><iframe id='youtube-frame' class='embed-responsive-item' src='" + element.video_url + "' frameborder='0' allowfullscreen></iframe></div><br>";
                $('#video-embed').append(newVideoEmbed);
            });
        } else {
            $('#video-embed').append("<div class='error-text'>No videos in this lecture.</div>");
        }
    };

    var changeMaterials = function(course){
        $('#material-embed .embed-responsive').remove();
        $('#material-embed .error-text').remove();
        $('#material-embed .material-pdf-download').remove();
        $('#material-embed br').remove();
        if (course.materials.length > 0) {
            course.materials.forEach(function (element) {
                var newMaterialEmbed = "<div class='embed-responsive embed-responsive-16by9'><object id='pdf-object' data='"+ element.material_file +"' type='application/pdf'><embed src='"+ element.material_file +"' type='application/pdf'/></object></div><br>";
                newMaterialEmbed = newMaterialEmbed + "<br><form class='material-pdf-download' id='pdf-form' method='get' action='"+ element.material_file +"'><button class='btn btn-primary' type='submit'><span class='glyphicon glyphicon-download'></span> Download</button> </form>"
                $('#material-embed').append(newMaterialEmbed);
            });
        } else {
            $('#material-embed').append("<div class='error-text'>No material in this lecture.</div>");
        }
    };

    $('.menu-row').on('click',function(e){
        var previous = $(this).closest("tbody").children(".active");
        previous.removeClass('active');
        $(this).addClass('active');
        var course_id = $(this).attr('id');
        var result = $.grep(courses, function(element){ return element.id == course_id; });
        if (result.length == 0) {
            console.log('course id not found');
        } else {
            changeVideos(result[0]);
            changeMaterials(result[0]);
            $('#pdf-form').attr('action', result[0].material);

            var nanoFrameParent = $('#nano-frame').parent();
            var newNanoFrame = "<iframe id='nano-frame' src='"+result[0].nano_url+"' width='100%' height='1800px' frameborder='0' ></iframe>";
            $('#nano-frame').remove();
            nanoFrameParent.append(newNanoFrame);
        }
    });

    $('.remove-video').on('click',function(e){
        $(this).parent().parent().remove();
    });

    $('.remove-material').on('click',function(e){
        $(this).parent().parent().remove();
    });

    $('.add-video').on('click', function(e) {
        var newVideoInput = '<div class="input-group">'+
        '<input class="form-control" name="videos[]" type="text" id="videos[]">'+
        '<span class="input-group-btn">'+
        '<a class="btn btn-link btn-lg red remove-video" type="button">'+
        '<span class="glyphicon glyphicon-minus-sign"></span>'+
        '</a></span></div>';

        var section = $(this).parent().append(newVideoInput);

        section.find('.remove-video').on('click', function() {
            $(this).parent().parent().remove();
        });
    });
});